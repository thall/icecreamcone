module.exports = function(grunt) {
	grunt.initConfig({
		jshint: {
			development: {
				options: {
					multistr: true
				},
				src: ['Gruntfile.js', 'bin/*.js', 'index.js', 'app/**/*.js', '!app/flavors/**/*']
			}
		}
	});
	grunt.registerTask('subtask', function(dir) {
		var done = this.async();
		function indent(str) {
			return '\t' + str.replace(/\n([^\n])/g, '\n\t$1');
		}

		grunt.util.spawn({
			grunt: true,
			args: [],
			opts: {
				cwd: dir
			}
		}, function(err, result) {
			console.log(indent(result.stdout));
			if(result.stderr) {
				console.error(indent(result.stderr));
			}
			if(err) {
				done(false);
			} else {
				done();
			}
		});
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');

	grunt.registerTask('default', [
		'jshint',
		'subtask:app/flavors/vanilla'
	]);
};
