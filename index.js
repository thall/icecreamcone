var cafescript = require('cafescript');
var express = require('express');
var path = require('path');

var Initializer = require('./app/lib/initializer.js');
var Loader = require('./app/lib/loader.js');
var flavor = require('./app/lib/flavor.js');
var error = require('./app/lib/error.js');
var db = require('./app/lib/db.js');

require('./app/lib/require.js')();

module.exports = function(config) {
	function params(req, data) {
		var params = req.exports;

		params.url = req.originalUrl;
		params.data = data;
		params.ctx = {
			prefix: config.prefix,
			brand: config.brand,
			pkg: config.pkg
		};

		return params;
	}

	var basename = flavor(config.flavor);
	var app = express();
	var ctx = {
		basename: basename,
		assets: express.static(path.resolve(basename, 'assets/')),
		template: function(req) {
			return {
				html: function(res, data) {
					res.render('html', params(req, data));
				},
				edit: function(res, data) {
					res.render('edit', params(req, data));
				},
				view: function(res, data) {
					res.render('view', params(req, data));
				}
			};
		}
	};
	var initialize = new Initializer(ctx);
	var loader = new Loader(app, ctx);

	function load() {
		app.engine('cafe', cafescript.render);
		app.set('view engine', 'cafe');

	//	db.connect(config.db);
		app.use('/assets/', function(req, res, next) {
			ctx.assets(req, res, next);
		});
		loader
			.templates()
			.middleware('core')
			.middleware('contrib')
			.middleware('custom')
			.controllers('core')
			.controllers('contrib')
			.controllers('custom');

		app.use(error(ctx));
	}

	initialize
		.then('core')
		.then('contrib')
		.then('custom')
		.init(config, function(err) {
			if(err) {
				throw err;
			} else {
				load();
			}
		});


	return app;
};
