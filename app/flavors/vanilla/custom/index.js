var twitter = require('./lib/twitter.js');
var github = require('./lib/github.js');

var async = require('async');

module.exports = {
	init: function(config, next) {
		async.parallel([
			twitter.init.bind(null, config.tw_consumer_key, config.tw_consumer_secret)
//			github.init.bind(null, config.git_client_key, config.git_client_secret)
		], next);
	}
};
