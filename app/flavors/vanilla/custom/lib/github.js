var Buffer = require('buffer').Buffer;
var https = require('https');

var Restful = require('icecreamcone/lib/restful.js');

var api = new Restful({
	hostname: 'api.github.com',
	headers: {
		'User-Agent': 'IceCreamCone'
	}
});

api.init = function(client, secret, done) {
	var data = JSON.stringify({
		client_secret: secret
	});
	var req = https.request({
		method: 'PUT',
		hostname: 'api.github.com',
		path: '/authorizations/clients/' + client,
		headers: {
			'User-Agent': 'IceCreamCone',
			'Content-Type': 'applications/json',
			'Content-Length': data.length
		}
	});
	req.on('error', done);
	req.on('response', function(res) {
		var result = new Buffer(0);
		res.on('data', function(data) {
			result = Buffer.concat([result, data]);
		});
		res.on('end', function() {
			var data = JSON.parse(result.toString('utf8'));
			console.log(data);
			api.authorize(data.token);
			done();
		});
	});
	req.write(data);
	req.end();
};

module.exports = api;
