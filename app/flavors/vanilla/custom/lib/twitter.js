var Buffer = require('buffer').Buffer;
var query = require('querystring');
var https = require('https');

var Restful = require('icecreamcone/lib/restful.js');

var api = new Restful({
	hostname: 'api.twitter.com',
	prefix: '/1.1'
});

api.init = function(token, secret, callback) {
	var auth = new Buffer(token + ':' + secret);
	var data = query.stringify({
		grant_type: 'client_credentials'
	});

	var req = https.request({
		method: 'post',
		hostname: 'api.twitter.com',
		path: '/oauth2/token',
		headers: {
			Authorization: 'Basic ' + auth.toString('base64'),
			'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
			'Content-Length': data.length
		}
	});
	req.on('error', function(err) {
		callback(err);
	});
	req.on('response', function(res) {
		var result = new Buffer(0);
		res.on('data', function(data) {
			result = Buffer.concat([result, data]);
		});
		res.on('end', function() {
			var data = JSON.parse(result.toString('utf8'));
			api.authorize('Bearer ' + data.access_token);
			callback(null);
		});
	});
	req.write(data);
	req.end();
};

module.exports = api;
