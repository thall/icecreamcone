var parser = require('body-parser');
var express = require('express');

var Article = require('icecreamcone/contrib/models/article.js');
var Author = require('icecreamcone/contrib/models/author.js');

var Profile = require('../models/profile.js');

module.exports = function(ctx) {
	var router = express.Router();

	router.route('/about/:id/')
		.get(function(req, res, next) {
			Article.findById(req.params.id, function(err, article) {
				if(err) {
					next(err);
				} else if(article) {
					if(!article.author || !article.author.profile) {
						article.author = {
							profile: {
								facebook: null
							}
						};
					}
					ctx.template(req).html(res, {
						type: 'content:about',
						about: {
							article: article,
							profile: article.author.profile
						}
					});
				} else {
					next();
				}
			});
		})
		.post(function(req, res, next) {
			res.send('update');
		})
		.delete(function(req, res, next) {
			Article.remove({ id: req.params.id }, function(err) {
				if(err) {
					next(err);
				} else {
					res.json({ status: 'success' });
				}
			});
		});
	router.route('/about/:id/edit/')
		.get(function(req, res, next) {
			res.send('edit');
		});

	return [
		parser.json(),
		parser.urlencoded({ extended: true }),
		router
	];
};
