var Sync = require('sync');
var Author = require('icecreamcone/contrib/models/author.js');

var facebook = require('../lib/facebook.js');
var twitter = require('../lib/twitter.js');
var github = require('../lib/github.js');

Author.schema.add({
	profile: Profile
});

function Profile(columns) {
	var fb_picture = facebook.get.future(null, '/:id/picture', {
		params: {
			id: columns.fb_id
		},
		query: {
			type: 'large',
			redirect: 'true'
		}
	});
	var tw_user = twitter.get.future(null, '/users/show.json', {
		query: {
			screen_name: columns.tw_name
		}
	});
	var git_user = github.get.future(null, '/users/:username', {
		params: {
			username: columns.git_name
		}
	});

	this.id = columns.id || columns._id;
	this.facebook = {
		img: fb_picture.result,
		url: columns.fb_url
	};
	this.twitter = {
		img: tw_user.result.profile_image_url.replace('_normal', ''),
		url: columns.tw_url
	};
	this.github = {
		img: git_user.result.avatar_url,
		url: git_user.result.html_url
	};
}

// TODO: migrate to db

var _profiles = [
	{
		_id: 1,
		fb_id: 1842492793,
		tw_name: 'Trevdog13',
		git_name: 'tjhall13',
		fb_url: 'https://www.facebook.com/trevor.hall.984',
		tw_url: 'https://twitter.com/Trevdog13'
	}
];

Profile.findById = function(id, callback) {
	Sync(function() {
		var profile;
		for(var i = 0; i < _profiles.length; i++) {
			if(id == _profiles[i]._id) {
				profile = _profiles[i];
				break;
			}
		}
		if(profile) {
			callback(null, new Profile(profile));
		} else {
			callback(null);
		}
	});
};

module.exports = Profile;
