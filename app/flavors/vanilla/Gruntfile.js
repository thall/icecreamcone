module.exports = function(grunt) {
	grunt.initConfig({
		jshint: {
			options: {
				multistr: true
			},
			server: {
				src: ['Gruntfile.js', 'custom/**/*.js']
			},
			client: {
				src: ['dev/js/**/*.js']
			}
		},
		less: {
			'assets/css/vanilla.min.css': ['dev/less/**/*.less']
		},
		uglify: {
			'assets/js/vanilla.min.js': ['dev/js/**/*.js']
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('server', ['jshint:server']);
	grunt.registerTask('client', ['jshint:client', 'less', 'uglify']);

	grunt.registerTask('default', ['server', 'client']);
};
