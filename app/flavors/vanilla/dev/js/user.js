$(function() {
	$('a[data-action="login"]').click(function(e) {
		var username = $('#username').val();
		var password = $('#password').val();
		var auth = { username: username, password: password };
		$.post($(this).attr('href'), auth, function(data) {
			if(data && data.success) {
				location.reload(true);
			} else {
				$('#username, #password').closest('.form-group').addClass('has-error');
				console.log('failed to log in');
			}
		}).fail(function(xhr, msg) {
			alert(msg);
		});
		e.preventDefault();
	});
	$('a[data-action="logout"]').click(function(e) {
		$.post($(this).attr('href'), { }, function(data) {
			if(data && data.success) {
				location.reload(true);
			} else {
				console.log('failed to log out');
			}
		}).fail(function(xhr, msg) {
			alert(msg);
		});
		e.preventDefault();
	});
});
