function updateBlogs(requests) {
	$.post(icecreamcone.api, { data: JSON.stringify({ blog: requests }) }, function(data) {
		if(data.success) {
			location.reload();
		} else {
			console.error(data.error);
		}
	});
}

$(function() {
	$('body').scrollspy({ target: ".post-history", offset: 80 });
	
	$('.post-history .nav li a').click(function() {
		$('html, body').animate({
			scrollTop: $($(this).attr('href')).offset().top - 80
		}, 400);
		return false;
	});
	
	var $blog = $('.post-editor');
	$blog.find('button[data-action="save"]').click(function(e) {
		var $form = $(this).closest('form');
		var request = {
			method: 'set',
			params: {
				id: $form.attr('data-post')
			}
		};
		
		if($form.find('input[name="title"]').val() != $form.find('input[name="title"]').attr('value')) {
			request.params.title = $form.find('input[name="title"]').val();
		}
		
		if($form.find('input[name="date"]').val() != $form.find('input[name="date"]').attr('value')) {
			request.params.date = $form.find('input[name="date"]').val();
		}
		
		request.params.text = CKEDITOR.instances[$form.find('textarea').attr('id')].getData();
		
		updateBlogs([request]);
		e.preventDefault();
	});
	$blog.find('button[data-action="remove"]').click(function(e) {
		var $form = $(this).closest('form');
		var request = {
			method: 'remove',
			params: {
				id: $form.attr('data-post')
			}
		};
		
		updateBlogs([request]);
		e.preventDefault();
	});
	
	$('.postfeed .add-post input[name="title"]').keyup(function() {
		if($('.postfeed .add-post input[name="url"]').val().substr(0, 6) == 'posts/') {
			var url;
			if($(this).val() === '') {
				url = 'posts/';
			} else {
				url = 'posts/' + $(this).val().toLowerCase().replace(/[ \t]+/, '-') + '/';
			}
			$('.postfeed .add-post input[name="url"]').val(url);
		}
	});
	
	$('.postfeed button[data-action="add"]').click(function(e) {
		var $form = $(this).closest('form');
		var request = {
			method: 'add',
			params: {
				title: $form.find('input[name="title"]').val(),
				url: $form.find('input[name="url"]').val(),
				date: $form.find('input[name="date"]').val(),
				text: CKEDITOR.instances[$form.find('textarea').attr('id')].getData()
			}
		};
		
		updateBlogs([request]);
		e.preventDefault();
	});
});
