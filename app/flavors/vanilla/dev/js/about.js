function updateAbout(requests) {
	$.post(icecreamcone.api, { data: JSON.stringify({ about: requests }) }, function(data) {
		if(data.success) {
			location.reload();
		} else {
			console.error(data.error);
		}
	});
}

$(function() {
	$('#about-edit button[type="submit"]').click(function(e) {
		var $form = $(this).closest('form');
		var $area = $form.find('textarea');
		var text = CKEDITOR.instances[$area.attr('id')].getData().trim();
		
		var request = {
			method: 'set',
			params: {
				id: $form.attr('data-about'),
				text: text
			}
		};
		updateAbout([request]);
		
		e.preventDefault();
	});
});
