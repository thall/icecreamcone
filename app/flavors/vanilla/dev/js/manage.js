function updateLinks(requests) {
	var url = icecreamcone.api;
	$.post(url, { data: JSON.stringify({ links: requests }) }, function(data) {
		if(data.success) {
			location.reload();
		} else {
			console.error(data.error);
		}
	});
}

function updatePages(requests) {
	var url = icecreamcone.api;
	$.post(url, { data: JSON.stringify({ pages: requests }) }, function(data) {
		if(data.success) {
			location.reload();
		} else {
			console.error(data.error);
		}
	});
}

$(function() {
	var $links = $('.link-editor');
	
	$links.find('button[data-action="add"]').click(function() {
		var $group = $(this).closest('.form-group');
		var request = {
			method: 'add',
			params: {
				id: $group.attr('data-link'),
				parent: $group.closest('.link-group').attr('data-link'),
				name: $group.find('input[name="name"]').val(),
				url: $group.find('input[name="url"]').val()
			}
		};
		
		updateLinks([request]);
	});
	$links.find('button[data-action="remove"]').click(function() {
		var $group = $(this).closest('.form-group');
		var request = {
			method: 'remove',
			params: {
				id: $group.attr('data-link')
			}
		};
		
		updateLinks([request]);
	});
	$links.find('button[data-action="save"]').click(function() {
		var $group = $(this).closest('.form-group');
		var request = {
			method: 'set',
			params: {
				id: $group.attr('data-link')
			}
		};
		var url = $group.find('input[name="url"]').val();
		if(url != $group.find('input[name="url"]').attr('value')) {
			request.params.url = url;
		}
		var name = $group.find('input[name="name"]').val();
		if(name != $group.find('input[name="name"]').attr('value')) {
			request.params.name = name;
		}
		
		updateLinks([request]);
	});
	$links.find('button[data-action="node"]').click(function() {
		var $links = $(this).closest('.form-group')
							.children('.link-group')
							.children('.form-group');
		var parent = $(this).closest('.link-group').attr('data-link');
		var previous = $(this).closest('.form-group').attr('data-link');
		var requests = [];
		$links.each(function(index, value) {
			var request;
			if($(value).attr('data-link')) {
				request = {
					method: 'set',
					params: {
						id: $(value).attr('data-link'),
						parent: parent,
						previous: previous
					}
				};
				
				requests.unshift(request);
			}
		});
		
		updateLinks(requests);
	});
	$links.find('button[data-action="leaf"]').click(function() {
		var $group = $(this).closest('.form-group');
		var $links = $('<div class="link-group" data-link="' + $group.attr('data-link') + '">');
		$links.append(
		'<div class="form-inline form-group">\
			<label>Name</label>\
			<input type="text" name="name" class="form-control">\
			<label>URL</label>\
			<input type="text" name="url" class="form-control">\
			<button class="btn btn-success" data-action="add"><span class="glyphicon glyphicon-plus-sign"></span></button>\
		</div>');
		$group.find('button, input[name="url"], label[for="url"]').remove();
		$links.find('button[data-action="add"]').click(function() {
			var $group = $(this).closest('.form-group');
			var request = {
				method: 'add',
				params: {
					id: $group.attr('data-link'),
					parent: $group.closest('.link-group').attr('data-link'),
					name: $group.find('input[name="name"]').val(),
					url: $group.find('input[name="url"]').val()
				}
			};
			
			updateLinks([request]);
		});
		$group.append($links);
	});
	
	var $content = $('.content-editor');
	$content.find('button[data-action="add"]').click(function() {
		var $values = $(this).closest('tr').children('td');
		var request = {
			method: 'add',
			params: {
				url: $($values[1]).find('input').val(),
				type: $($values[2]).find('input').val(),
				content: $($values[3]).find('input').val()
			}
		};
		
		updatePages([request]);
	});
	$content.find('button[data-action="remove"]').click(function() {
		var $values = $(this).closest('tr').children('td');
		var request = {
			method: 'remove',
			params: {
				id: $($values[0]).text()
			}
		};
		
		updatePages([request]);
	});
	$content.find('button[data-action="save"]').click(function() {
		var $values = $(this).closest('tr').children('td');
		var request = {
			method: 'set',
			params: {
				id: $($values[0]).text()
			}
		};
		
		function input(index) {
			return $($values[index]).find('input');
		}
		
		if(input(1).attr('value') != input(1).val()) {
			request.params.url = input(1).val();
		}
		if(input(2).attr('value') != input(2).val()) {
			request.params.type = input(2).val();
		}
		if(input(3).attr('value') != input(3).val()) {
			request.params.content = input(3).val();
		}
		
		updatePages([request]);
	});
});
