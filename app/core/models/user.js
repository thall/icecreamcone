var Sync = require('sync');

function User(columns) {
	this._columns = columns;

	this.id = columns.id || columns._id;
	this.username = columns.username;
	this.name = columns.name;
}

function add(name, type) {
	if(typeof type == 'function') {
		column = name.toLowerCase() + '_id';
		Object.defineProperty(User.prototype, name, {
			get: function() {
				var instance = type.findById.sync(type, this._columns[column]);
				Object.defineProperty(this, name, {
					value: instance
				});
				return instance;
			}
		});
	} else {
		column = name.toLowerCase();
		// TODO: handle built in types
	}
	// TODO: add column to db
}

User.schema = {
	add: function(keys) {
		for(var key in keys) {
			add(key, keys[key]);
		}
	}
};

// TODO: migrate to db

var _users = [
	{
		_id: 1,
		username: 'thall',
		name: 'Trevor Hall',
		profile_id: 1
	}
];

User.findById = function(id, callback) {
	var Model = this;
	Sync(function() {
		var user;
		for(var i = 0; i < _users.length; i++) {
			if(id == _users[i]._id) {
				user = _users[i];
				break;
			}
		}
		if(user) {
			callback(null, new Model(user));
		} else {
			callback(null);
		}
	});
};

module.exports = User;
