var express = require('express');
var parser = require('body-parser');

var Component = require('../models/component.js');

module.exports = function(ctx) {
	var router = express.Router();

	router.route('/component/:id/')
		.get(function(req, res, next) {
			Component.findById(req.params.id, function(err, component) {
				if(err) {
					next(err);
				} else {
					res.json(component);
				}
			});
		})
		.post(function(req, res, next) {
			res.send('update');
		})
		.delete(function(req, res, next) {
			Component.remove({ id: req.params.id }, function(err) {
				if(err) {
					next(err);
				} else {
					res.json({ status: 'success' });
				}
			});
		});
	router.route('/component/:id/edit/')
		.get(function(req, res, next) {
			res.send('edit');
		});

	return [
		parser.json(),
		parser.urlencoded({ extended: true }),
		router
	];
};
