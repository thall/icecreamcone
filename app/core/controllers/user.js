var express = require('express');
var parser = require('body-parser');

var User = require('../models/user.js');

module.exports = function(ctx) {
	var router = express.Router();

	router.route('/users/')
		.get(function(req, res, next) {
			res.send('users');
		})
		.post(function(req, res, next) {
			res.send('login');
		});
	router.route('/users/:id/')
		.get(function(req, res, next) {
			User.findById(req.params.id, function(err, user) {
				if(err) {
					next(err);
				} else if(user) {
					ctx.template(req).html(res, {
						type: 'user',
						user: user
					});
				} else {
					next();
				}
			});
		})
		.post(function(req, res, next) {
			res.send('update');
		})
		.delete(function(req, res, next) {
			User.remove({ id: req.params.id }, function(err) {
				if(err) {
					next(err);
				} else {
					res.json({ status: 'success' });
				}
			});
		});
	router.route('/users/:id/edit/')
		.get(function(req, res, next) {
			res.send('edit');
		});

	return [
		parser.json(),
		parser.urlencoded({ extended: true }),
		router
	];
};
