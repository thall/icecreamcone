var express = require('express');
var parser = require('body-parser');

var Content = require('../models/content.js');

module.exports = function(ctx) {
	var router = express.Router();

	router.route('/content/:id/')
		.get(function(req, res, next) {
			Content.findById(req.params.id, function(err, content) {
				if(err) {
					next(err);
				} else if(content) {
					ctx.template(req).html(res, {
						type: 'content',
						content: content
					});
				} else {
					next();
				}
			});
		})
		.post(function(req, res, next) {
			res.send('update');
		})
		.delete(function(req, res, next) {
			Content.remove({ id: req.params.id }, function(err) {
				if(err) {
					next(err);
				} else {
					res.json({ status: 'success' });
				}
			});
		});
	router.route('/content/:id/edit/')
		.get(function(req, res, next) {
			res.send('edit');
		});

	return [
		parser.json(),
		parser.urlencoded({ extended: true }),
		router
	];
};
