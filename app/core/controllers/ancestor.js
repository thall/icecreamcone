var express = require('express');
var parser = require('body-parser');

var Ancestor = require('../models/ancestor.js');

module.exports = function(ctx) {
	var router = express.Router();

	router.route('/ancestor/:id/')
		.get(function(req, res, next) {
			Ancestor.findById(req.params.id, function(err, ancestor) {
				if(err) {
					next(err);
				} else if(ancestor) {
					ctx.template(req).html(res, {
						type: 'ancestor',
						ancestor: ancestor
					});
				} else {
					next();
				}
			});
		})
		.post(function(req, res, next) {
			res.send('update');
		})
		.delete(function(req, res, next) {
			Ancestor.remove({ id: req.params.id }, function(err) {
				if(err) {
					next(err);
				} else {
					res.json({ status: 'success' });
				}
			});
		});
	router.route('/ancestor/:id/edit/')
		.get(function(req, res, next) {
			res.send('edit');
		});

	return [
		parser.json(),
		parser.urlencoded({ extended: true }),
		router
	];
};
