var express = require('express');
var parser = require('body-parser');

var Blog = require('../models/blog.js');

module.exports = function(ctx) {
	var router = express.Router();

	router.route('/blogs/')
		.get(function(req, res, next) {
			Blog.find(function(err, blogs) {
				if(err) {
					next(err);
				} else {
					ctx.template(req).html(res, {
						type: 'ancestor:blog',
						blogs: blogs
					});
				}
			});
		});
	router.route('/blogs/:id/')
		.get(function(req, res, next) {
			Blog.findById(req.params.id, function(err, blog) {
				if(err) {
					next(err);
				} else if(blog) {
					ctx.template(req).html(res, {
						type: 'content:blog',
						blog: blog
					});
				} else {
					next();
				}
			});
		})
		.post(function(req, res, next) {
			res.send('update');
		})
		.delete(function(req, res, next) {
			Blog.remove({ id: req.params.id }, function(err) {
				if(err) {
					next(err);
				} else {
					res.json({ status: 'success' });
				}
			});
		});
	router.route('/blogs/:id/edit/')
		.get(function(req, res, next) {
			res.send('edit');
		});

	return [
		parser.json(),
		parser.urlencoded({ extended: true }),
		router
	];
};
