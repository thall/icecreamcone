var express = require('express');
var parser = require('body-parser');

var Alias = require('../models/alias.js');

module.exports = function(ctx) {
	var router = express.Router();

	router.route('/alias/:id/')
		.get(function(req, res, next) {
			Alias.findById(req.params.id, function(err, alias) {
				if(err) {
					next(err);
				} else {
					res.json(alias);
				}
			});
		})
		.post(function(req, res, next) {
			res.send('update');
		})
		.delete(function(req, res, next) {
			Alias.remove({ id: req.params.id }, function(err) {
				if(err) {
					next(err);
				} else {
					res.json({ status: 'success' });
				}
			});
		});
	router.route('/alias/:id/edit/')
		.get(function(req, res, next) {
			res.send('edit');
		});

	return [
		parser.json(),
		parser.urlencoded({ extended: true }),
		router
	];
};
