// var db = require('icecreamcone/lib/db.js');

function Alias(columns) {
	this.id = columns.id || columns._id;
	this.url = columns.url;
	this.redirect = columns.redirect;
	this.location = columns.location;
}

/*
Alias.findOne = function(params, callback) {
	var conditions = '';
	var parameters = [];
	for(var param in params) {
		parameters.push(param);
		parameters.push(params[param]);
		conditions += '?? = ? AND ';
	}
	if(parameters.length) {
		conditions = conditions.slice(0, -5);
	}
	db.getConnection(function(err, conn) {
		if(err) {
			callback(err);
		} else {
			conn.query('SELECT _id, url, redirect, location FROM Alias WHERE ' + conditions + ' LIMIT 1;', parameters, function(err, results) {
				conn.release();

				if(err) {
					callback(err);
				} else {
					if(results.length) {
						var alias = new Alias(results[0]);
						callback(null, alias);
					} else {
						callback(null);
					}
				}
			});
		}
	});
};
*/

// TODO: migrate to db

var _aliases = [
	{
		_id: 1,
		url: '/',
		redirect: false,
		location: '/about/1/'
	}, {
		_id: 2,
		url: '/posts/',
		redirect: false,
		location: '/blogs/'
	}, {
		_id: 3,
		url: '/posts/first-post/',
		redirect: false,
		location: '/blogs/1/'
	}, {
		_id: 4,
		url: '/posts/second-post/',
		redirect: false,
		location: '/blogs/2/'
	}, {
		_id: 5,
		url: '/posts/idea-mysquirrel/',
		redirect: false,
		location: '/blogs/3/'
	}
];

Alias.findOne = function(parameters, callback) {
	var alias;
	for(var i = 0; i < _aliases.length; i++) {
		for(var param in parameters) {
			alias = _aliases[i];
			if(parameters[param] != alias[param]) {
				alias = null;
				break;
			}
		}
		if(alias) {
			break;
		}
	}
	if(alias) {
		callback(null, new Alias(alias));
	} else {
		callback(null);
	}
};

module.exports = Alias;
