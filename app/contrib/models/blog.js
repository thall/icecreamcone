function Blog(columns) {
	this.id = columns.id || columns._id;
	this.title = columns.title;
	this.body = columns.body;
	this.date = columns.date;
	this.url = columns.url;
}

// TODO: migrate to db

var _blogs = [
	{
		_id: 1,
		title: 'First Post',
		body: '<p>This is my first post. Currently I have nothing to say but I\'m sure that will change soon.</p>',
		date: new Date(2015, 4, 31),
		url: '/posts/first-post/'
	}, {
		_id: 2,
		title: 'Second Post',
		body: '<p>This is a test post for the new CMS. This site is now hosted by IceCreamCone.</p>',
		date: new Date(2015, 9, 31),
		url: '/posts/second-post/'
	}, {
		_id: 3,
		title: 'Idea: MySquirrel',
		body: '<p>While considering languages and tools to replace the current StudyBinder server project I began experimenting with NodeJS. &nbsp;Recently I have been working with mongoose in node. &nbsp;It provides an intuitive API to work with the MongoDB my design studio team has been working with. &nbsp;The problem is that on the StudyBinder server, we use MySQL so I had an idea. &nbsp;What if I was to design a similar API for the MySQL database. &nbsp;I could use various techniques to support models like mongoose does. &nbsp;Thus MySquirrel was born, and with that my addiction to software themed puns became aparent.</p>',
		date: new Date(2015, 10, 24),
		url: '/posts/idea-mysquirrel/'
	}
];

Blog.findById = function(id, callback) {
	var blog;
	for(var i = 0; i < _blogs.length; i++) {
		if(_blogs[i]._id == id) {
			blog = _blogs[i];
			break;
		}
	}
	if(blog) {
		callback(null, new Blog(blog));
	} else {
		callback(null);
	}
};

Blog.find = function(parameters, callback) {
	if(typeof parameters == 'function') {
		callback = parameters;
		parameters = { };
	}
	callback(null, _blogs.map(function(blog) { return new Blog(blog); }));
};

module.exports = Blog;
