var Sync = require('sync');

var Author = require('icecreamcone/contrib/models/author.js');

function Article(columns) {
	this.id = columns.id || columns._id;
	this.html = columns.html;
	this.author = Author.findById.sync(Author, columns.author_id);
}

// TODO: migrate to db

var _articles = [
	{
		_id: 1,
		author_id: 1,
		html: 
'<h1>\
	About Me\
</h1>\
<p>\
	My name is Trevor Hall. I am a Senior Computer Engineering\
	Student at the University of Nebraska-Lincoln. I am from Norwalk\
	Iowa and have been attending the University of Nebraska-Lincoln\
	since the Fall of 2012. My interests are in programming language\
	design, virtual machines and operating systems.\
</p>\
<p>\
	In 2013 I joined my friend Matt Gustin in a business he started\
	to give us a platform to design and innovate ideas of ours. With\
	his focus being on audio equipment I began doing development on a\
	website/app to help his fraternity. StudyBinder was born as a\
	website to help his fraternity manage members\' study hours. The\
	project evolved and is now a full mobile application currently in\
	development.\
</p>\
<p>\
	Since development began I have started other projects small and\
	large. My current focus outside of school and StudyBinder is a\
	project called Givingest. Givingest has been a wonderful\
	oppurtunity to get experience building mobile applications and\
	designing websites.\
</p>\
<div class="divider"></div>\
<h4>\
	Email: trevor.jacob.hall@gmail.com\
</h4>'
	}
];

Article.findById = function(id, callback) {
	Sync(function() {
		var article;
		for(var i = 0; i < _articles.length; i++) {
			if(_articles[i]._id == id) {
				article = _articles[i];
				break;
			}
		}
		if(article) {
			callback(null, new Article(article));
		} else {
			callback(null);
		}
	});
};

module.exports = Article;
