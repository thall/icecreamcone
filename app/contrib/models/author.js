var util = require('util');

var User = require('icecreamcone/core/models/user.js');

function Author(columns) {
	User.call(this, columns);
}

util.inherits(Author, User);

Author.schema = User.schema;
Author.findById = User.findById;

module.exports = Author;
