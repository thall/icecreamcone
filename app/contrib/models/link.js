function Link(columns) {
	this.id = columns.id || columns._id;
	this.name = columns.name;
	this.href = columns.href;
	this.parent = columns.parent;
	this.prev = columns.prev;
}

var _links = [
	{
		_id: 1,
		name: 'About',
		href: '/',
		parent: null,
		prev: null
	}, {
		_id: 2,
		name: 'Posts',
		href: '/posts/',
		parent: null,
		prev: 1
	}, {
		_id: 3,
		name: 'Projects',
		href: '/projects/',
		parent: null,
		prev: 2
	}, {
		_id: 4,
		name: 'StudyBinder',
		href: '/projects/studybinder/',
		parent: 3,
		prev: null
	}, {
		_id: 5,
		name: 'Givingest',
		href: 'http://www.givingest.com',
		parent: 3,
		prev: 4
	}, {
		_id: 6,
		name: 'Nopache',
		href: '/projects/nopache/',
		parent: 3,
		prev: 5
	}
];

Link.find = function(parameters, callback) {
	if(typeof parameters == 'function') {
		callback = parameters;
		parameters = { };
	}
	callback(null, _links.map(function(link) { return new Link(link); }));
};

module.exports = Link;
