var Link = require('../models/link.js');

function init(set) {
	var links = set.reduce(function(map, link) {
		map[link.id] = link;
		return map;
	}, { });

	var parent, prev, root;
	for(var id in links) {
		if(links[id].parent && !links[id].prev) {
			links[links[id].parent].child = links[id];
		} else if(links[id].prev) {
			prev = links[links[id].prev];
			prev.next = links[id];
		} else if(!links[id].parent) {
			root = links[id];
		}
	}

	return root;
}

function order(node) {
	var links, link;
	if(node.next) {
		links = order(node.next);
	} else {
		links = [];
	}
	link = {
		name: node.name,
		href: node.href
	};
	if(node.child) {
		link.links = order(node.child);
	}
	links.unshift(link);
	return links;
}

module.exports = function(ctx) {
	return function(req, res, next) {
		Link.find(function(err, links) {
			if(err) {
				next(err);
			} else {
				req.exports.links = order(init(links));
				next();
			}
		});
	};
};
