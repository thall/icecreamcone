var HttpStatus = require('../../lib/httpstatus.js');
var Alias = require('../models/alias.js');

module.exports = function(ctx) {
	return function(req, res, next) {
		Alias.findOne({ url: req.url }, function(err, alias) {
			if(err) {
				next(err);
			} else {
				if(alias) {
					if(alias.redirect) {
						res.redirect(alias.location);
					} else {
						req.url = alias.location;
						next();
					}
				} else {
					next(new HttpStatus(404, req.originalUrl));
				}
			}
		});
	};
};
