var Buffer = require('buffer').Buffer;
var query = require('querystring');
var https = require('https');

function Restful(options) {
	var _authorization;

	if(!options.hostname) {
		throw new Error('hostname must be provided');
	}
	var config = {
		prefix: '',
		headers: { },
		response: function(result) {
			return JSON.parse(result.toString('utf8'));
		}
	};
	for(var prop in options) {
		config[prop] = options[prop];
	}

	function parameterize(path, args) {
		var token = /:[a-zA-Z_]+/g;
		if(args.params) {
			for(var param in args.params) {
				path = path.replace(':' + param, args.params[param]);
			}
		}
		path = path.replace(token, undefined);
		if(args.query) {
			path += '?' + query.stringify(args.query);
		}
		return config.prefix + path;
	}

	function incoming(request, callback) {
		request.on('error', function(err) {
			callback(err);
		});
		request.on('response', function(res) {
			var result;
			if(res.statusCode == 302 || res.statusCode == 304) {
				result = res.headers.Location || res.headers.location;
				callback(null, result);
			} else if(res.statusCode == 200) {
				result = new Buffer(0);
				res.on('data', function(data) {
					result = Buffer.concat([result, new Buffer(data)]);
				});
				res.on('end', function() {
					callback(null, config.response(result));
				});
			} else {
				callback(new Error(res.statusCode));
			}
		});
	}

	this.authorize = function(token) {
		config.headers.Authorization = token;
	};

	this.get = function(path, args, callback) {
		try {
			var req = https.request({
				method: 'GET',
				hostname: config.hostname,
				path: parameterize(path, args),
				headers: config.headers
			});
			incoming(req, callback);
			req.end();
		} catch(err) {
			callback(err);
		}
	};
}

module.exports = Restful;
