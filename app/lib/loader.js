var express = require('express');
var glob = require('glob');
var path = require('path');

function Loader(app, ctx) {
	app.use(function(req, res, next) {
		req.exports = { };
		next();
	});

	function middleware(rel) {
		var directory = path.resolve(rel, 'middleware/');
		var middleware = glob.sync('*.js', {
			cwd: directory
		});
		middleware.forEach(function(file) {
			var middleware = require(directory + '/' + file);
			app.use(middleware(ctx));
		});
	}
	function controllers(rel) {
		var directory = path.resolve(rel, 'controllers/');
		var controllers = glob.sync('*.js', {
			cwd: directory
		});
		controllers.forEach(function(file) {
			var controller = require(directory + '/' + file);
			app.use(controller(ctx));
		});
	}

	this.basename = function(pathname) {
		basename = path.resolve(pathname);
		return this;
	};
	this.middleware = function(type) {
		switch(type) {
			case 'core':
				middleware(path.resolve(__dirname, '../core/'), ctx);
				break;
			case 'contrib':
				middleware(path.resolve(__dirname, '../contrib/'), ctx);
				break;
			case 'custom':
				middleware(path.resolve(ctx.basename, 'custom/'), ctx);
				break;
		}
		return this;
	};
	this.controllers = function(type) {
		switch(type) {
			case 'core':
				controllers(path.resolve(__dirname, '../core/'), ctx);
				break;
			case 'contrib':
				controllers(path.resolve(__dirname, '../contrib/'), ctx);
				break;
			case 'custom':
				controllers(path.resolve(ctx.basename, 'custom/'), ctx);
				break;
		}
		return this;
	};
	this.templates = function() {
		app.set('views', path.resolve(ctx.basename, 'templates/'));
		return this;
	};
}

module.exports = Loader;
