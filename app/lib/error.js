var util = require('util');

var HttpStatus = require('./httpstatus.js');

module.exports = function(ctx) {
	var missing = function(req, res, next) {
		next(new HttpStatus(404, req.originalUrl));
	};

	var error = function(err, req, res, next) {
		if(err instanceof HttpStatus) {
			if(err.status == 500) {
				console.error(err.status + ':', err.data);
			}
			res.status(err.status).send(util.inspect(err.data));
		} else {
			console.error(err.stack);
			res.status(500).send(err.message);
		}
	};

	return [
		missing,
		error
	];
};
