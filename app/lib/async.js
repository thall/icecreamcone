function forEach(array, fn, callback, index) {
	if(index < array.length) {
		fn(function(err) {
			if(err) {
				callback(err);
			} else {
				forEach(array, fn, callback, index + 1);
			}
		}, array[index], index, array);
	} else {
		callback(null);
	}
}

Array.prototype.forEachAsync = function(fn, callback) {
	forEach(this, fn, callback, 0);
};
