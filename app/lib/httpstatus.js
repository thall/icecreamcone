function HttpStatus(status, data) {
	this.status = status;
	this.data = data;
}

module.exports = HttpStatus;
