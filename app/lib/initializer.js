var glob = require('glob');
var path = require('path');

require('./async.js');

function Initializer(ctx) {
	var scripts = [];

	function init(rel, config, callback) {
		var directory = rel;
		var index = glob.sync('index.js', {
			cwd: directory
		});
		index.forEachAsync(function(next, file) {
			var index = require(directory + '/' + file);
			index.init(config, next);
		}, callback);
	}

	this.then = function(type) {
		switch(type) {
			case 'core':
				scripts.push(path.resolve(__dirname, '../core/'));
				break;
			case 'contrib':
				scripts.push(path.resolve(__dirname, '../contrib/'));
				break;
			case 'custom':
				scripts.push(path.resolve(ctx.basename, 'custom/'));
				break;
		}
		return this;
	};

	this.init = function(config, callback) {
		scripts.forEachAsync(function(next, rel) {
			init(rel, config, next);
		}, callback);
	};
}

module.exports = Initializer;
