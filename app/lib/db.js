var mysql = require('mysql');

var pool;

module.exports = {
	getConnection: function(callback) {
		if(pool) {
			pool.getConnection(callback);
		} else {
			callback(new Error('Connection pool has not been initialized.'));
		}
	},
	connect: function(options) {
		pool = mysql.createPool(options);
	}
};
