var Module = require('module');
var assert = require('assert');
var path = require('path');

var _require = Module.prototype.require;

function _stub(pathname) {
	assert(pathname, 'missing path');
	assert(typeof pathname === 'string', 'path must be a string');

	var regex = /^icecreamcone\/(.+)$/;
	var mod = pathname.match(regex);
	if(mod) {
		pathname = path.resolve(__dirname, '../', mod[1]);
		return _require.call(this, pathname);
	} else {
		return _require.call(this, pathname);
	}
}

module.exports = function() {
	Module.prototype.require = _stub;
};
